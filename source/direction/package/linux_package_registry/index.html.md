---
layout: markdown_page
title: "Category Vision - Linux Package Registry"
---

- TOC
{:toc}

## Linux Package Registry

Linux distros depend on linux package regisitries for distribution of installable software. By supporting Debian and RPM we will cater to a big chunk of the market and allow [Systems Administrator](https://design.gitlab.com/research/personas#persona-sidney) tasks to be internalized.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Linux%20Package%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1290) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

First we need to complete the MVC which will allow users to provision and deploy Linux packages. [gitlab-ee#5835](https://gitlab.com/gitlab-org/gitlab-ee/issues/5835) focuses on Debian and [gitlab-ee#5932](https://gitlab.com/gitlab-org/gitlab-ee/issues/5932) focuses on RPM. This will allow users, both internal and external, to start using the product and provide valuable feedback for future iterations.

## Maturity Plan
This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [MVC for .deb Linux Package Registry](https://gitlab.com/gitlab-org/gitlab-ee/issues/5835)
- [MVC for .rpm Linux Package Registry](https://gitlab.com/gitlab-org/gitlab-ee/issues/5932)

We may only select one of these two to start with.

## Competitive Landscape

- [JFrog](https://jfrog.com/) currently supports both [Debian](https://www.jfrog.com/confluence/display/RTF/Debian+Repositories) and [RPM](https://www.jfrog.com/confluence/display/RTF/RPM+Repositories)
- [Sonatype Nexus](https://www.sonatype.com/nexus-repository-sonatype)

## Top Customer Success/Sales Issue(s)

There are currently no customer success or sales issues for this category.

## Top Customer Issue(s)

As we have not yet releaseed the MVC, there are no customer issues.

## Top Internal Customer Issue(s)

The Distribution team utilizes an external tool for building Linux packages. By offering support for Debian and RPM we can begin to remove that external dependency. 

## Top Vision Item(s)

Our vision begins with implementing [gitlab-ee#5835](https://gitlab.com/gitlab-org/gitlab-ee/issues/5835) and [gitlab-ee#5932](https://gitlab.com/gitlab-org/gitlab-ee/issues/5932) which discuss adding support for Debian and RPM respectively.
